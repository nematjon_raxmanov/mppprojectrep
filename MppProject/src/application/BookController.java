package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import business.Author;
import business.Book;
import dataaccess.DataAccessFacade;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class BookController {

	@FXML
	private Button btnAddAuthor;
	@FXML
	private Button btnDeleteAuthor;
	
	@FXML
	private Button btnAddBook;
	
	@FXML
	private TextField txtIsbn;
	
	@FXML
	private TextField txtTitle;
	
	@FXML
	private TextField txtCheckoutDay;
	
	@FXML
	private TextField txtCopyCount;
	
	@FXML
	private ListView<Author> lstAuthor;
	
	@FXML
	private Label lblCopiesCount;

	@FXML
	private Label lblValidation;
	
	boolean bResult;
	boolean bOnlyCopy; 
	Book currBook;
	DataAccessFacade da;
	
	public void initialize()
	{
		this.bResult=false;
		this.bOnlyCopy=false;
		lblCopiesCount.setText("Copies count");
		da=new DataAccessFacade();
		
		txtIsbn.textProperty().addListener((observable, oldValue, newValue) -> 
		{
			if(this.bOnlyCopy)
				return;
			
			if(newValue.isBlank() || newValue.isEmpty())
			{
				lblValidation.setText("");
			}
			else if(da.isISBNExist(newValue))
			{
				lblValidation.setText("Already exist!");
				lblValidation.setTextFill(Color.RED);
			}
			else
			{
				lblValidation.setText("Valid");
				lblValidation.setTextFill(Color.GREEN);
			}
		});
	}
	
	public static int tryParseInt(String value)
	{
	    try 
	    {
	        return Integer.parseInt(value);
	    } 
	    catch (NumberFormatException e) 
	    {
	        return 0;
	    }
	}
	
	public void AddBook(ActionEvent event) throws IOException 
	{
		int cnt=tryParseInt(txtCopyCount.getText());
		if(cnt<=0)
		{
			MessageBox.Show("Error", "Book copies must be at least 1");
			return;
		}
		
		if(this.bOnlyCopy)
		{
			for(int i=0;i<cnt;i++)
				this.currBook.addCopy();
			this.bResult=da.saveBookCopies(this.currBook);
		}
		else
		{
			String isbn=txtIsbn.getText();
			if(isbn.isEmpty() || isbn.isBlank())
			{
				MessageBox.Show("Error","Please enter correct ISBN!");
				return;
			}
			
			String title=txtTitle.getText();
			if(title.isEmpty() || title.isBlank())
			{
				MessageBox.Show("Error","Please enter correct Title!");
				return;
			}
			
			int checkDay=tryParseInt(txtCheckoutDay.getText());
			if(checkDay<=0)
			{
				MessageBox.Show("Error", "Book checkout days must be enter!");
				return;
			}
			
			if(this.lstAuthor.getItems().size()==0)
			{
				MessageBox.Show("Error", "Book authors must be enter!");
				return;
			}
			
			List<Author> lst=new ArrayList<>();
			for(Author a:this.lstAuthor.getItems())
			{
				lst.add(a);
			}
			
			this.currBook=new Book(isbn,title,checkDay,lst);
			this.bResult=da.saveNewBook(this.currBook);
			if(!this.bResult)
			{
				MessageBox.Show("Error", "This ISBN already used. Please enter different ISBN!");
				return;
			}
		}
		
		Stage stage = (Stage) btnAddBook.getScene().getWindow();
		stage.close();
	}
	
	public void AddAuthor(ActionEvent event) throws IOException 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AuthorForm.fxml"));
		Parent root = (Parent)loader.load();
		AuthorController controller = (AuthorController)loader.getController();
		Scene scene = new Scene(root,600,700);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		Stage st=new Stage();
		st.setScene(scene);
		st.setTitle("Add author");
		st.getIcons().add(new Image("/data/library.png"));
		st.initModality(Modality.APPLICATION_MODAL);
		st.showAndWait();
		
		Author a=controller.getAuthor();
		if(a!=null)
		{
			this.lstAuthor.getItems().add(a);
		}
	}
	
	public void RemoveAuthor(ActionEvent event) throws IOException 
	{
		if(lstAuthor.getSelectionModel()!=null)
		{
			Author a=lstAuthor.getSelectionModel().getSelectedItem();
			if(a!=null)
			{
				this.lstAuthor.getItems().remove(a);
			}
		}
	}
	
	public void EditCopy(Book b)
	{
		this.bOnlyCopy=true;
		
		lblCopiesCount.setText("New copies count");
		
		btnAddAuthor.setVisible(false);
		btnDeleteAuthor.setVisible(false);
		
		txtIsbn.setText(b.getIsbn());
		txtIsbn.setDisable(true);
		
		txtTitle.setText(b.getTitle());
		txtTitle.setDisable(true);
		
		txtCheckoutDay.setText(String.valueOf(b.getMaxCheckoutLength()));
		txtCheckoutDay.setDisable(true);
		
		txtCopyCount.setText("1");
		btnAddBook.setText("Add book copy");
		this.lstAuthor.getItems().addAll(b.getAuthors());
		
		this.currBook=b;
		
	}
	
	public boolean getResult()
	{
		return this.bResult;
	}
}
