package application;

import business.Address;
import business.LibraryMember;
import dataaccess.DataAccessFacade;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javafx.stage.Stage;

public class AddMemberController
{	
	@FXML
	private Button btnAddMember;
	@FXML
	private Label txtMemberID;
	@FXML
	private TextField txtFirstName;
	@FXML
	private TextField txtLastName;
	@FXML
	private TextField txtPhone;
	@FXML
	private TextField txtStreet;
	@FXML
	private TextField txtCity;
	@FXML
	private TextField txtState;
	@FXML
	private TextField txtZipCode;
	@FXML
	private Label lblError;
	
	private boolean bResult;
	private boolean bEditMode;
	DataAccessFacade da;
	String memberID;
	
	public void initialize() 
	{
		da=new DataAccessFacade();
		this.memberID=da.generateMemberId();
		this.lblError.setText("");
		this.bResult=false;
		this.bEditMode=false;
		this.btnAddMember.setText("Add new member");
		this.txtMemberID.setText(this.memberID);
	}
	
	public void AddMember(ActionEvent event) 
	{
		String fname=txtFirstName.getText();
		if(fname.isEmpty() || fname.isBlank())
		{
			lblError.setText("Please enter correct first name!");
			return;
		}
		
		String lname=txtLastName.getText();
		if(lname.isEmpty() || lname.isBlank())
		{
			lblError.setText("Please enter correct last name!");
			return;
		}
		
		String phone=txtPhone.getText();
		if(phone.isEmpty() || phone.isBlank() || phone.length()!=12)
		{
			lblError.setText("Please enter correct phone number! Format=XXX-XXX-XXXX");
			return;
		}
		
		String street=txtStreet.getText();
		if(street.isEmpty() || street.isBlank())
		{
			lblError.setText("Please enter correct street!");
			return;
		}
		
		String city=txtCity.getText();
		if(city.isEmpty() || city.isBlank())
		{
			lblError.setText("Please enter correct city!");
			return;
		}
		
		String state=txtState.getText();
		if(state.isEmpty() || state.isBlank())
		{
			lblError.setText("Please enter correct state!");
			return;
		}
		
		String zip=txtZipCode.getText();
		if(zip.isEmpty() || zip.isBlank() || zip.matches("\\d{10}|\\d{11}"))
		{
			lblError.setText("Please enter correct zip number!");
			return;
		}
		
		LibraryMember libMember=new LibraryMember(this.memberID, fname, lname, phone, new Address(street, city, state, zip));
		if(!this.bEditMode)
		{
			
			da.saveNewMember(libMember);
			this.bResult=true;
		}
		else
		{
			this.bResult=da.editLibraryMember(libMember);
		}
		
		Stage stage = (Stage) btnAddMember.getScene().getWindow();
	    stage.close();
	}
	
	public boolean getResult()
	{
		return this.bResult;
	}
		
	public void EditMember(LibraryMember lib)
	{
		this.bEditMode=true;
		this.memberID=lib.getMemberId();
		this.btnAddMember.setText("Edit member");
		
		this.txtMemberID.setText(this.memberID);
		this.txtFirstName.setText(lib.getFirstName());
		this.txtLastName.setText(lib.getLastName());
		this.txtPhone.setText(lib.getTelephone());
		
		Address adr=lib.getAddress();
		this.txtStreet.setText(adr.getStreet());
		this.txtCity.setText(adr.getCity());
		this.txtState.setText(adr.getState());
		this.txtZipCode.setText(adr.getZip());
		
	}
}
