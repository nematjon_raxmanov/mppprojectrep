package application;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import business.Book;
import business.BookCopy;
import business.BookRecord;
import business.CheckOutEntry;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.util.Callback;

public class BookSearchController {
		
	@FXML
	private TableView<BookRecord> tbView;
	
	@FXML 
	private TableColumn<BookRecord, String> tbcol1;
	
	@FXML 
	private TableColumn<BookRecord, String> tbcol2;
	
	@FXML 
	private TableColumn<BookRecord, String> tbcol3;
	
	@FXML
	private TableColumn<BookRecord, String> tbcol4;
	
	@FXML
	private TableColumn<BookRecord, String> tbcol5;
	
	@FXML
	private TableColumn<BookRecord, String> tbcol6;
	
	@FXML
	private TableColumn<BookRecord, String> tbcol7;
	
	@FXML
	private TableColumn<BookRecord, String> tbcol8;
	
	public void initialize() 
	{
		
	}
	
	public void setTableViewValues(Book book) {
		List<BookRecord> books = new ArrayList<>();
		
		for(BookCopy b: book.getCopies()) {
			
			
			BookRecord br = new BookRecord(book.getIsbn(), book.getTitle(), String.valueOf(b.getCopyNum()), b.isAvailable() ? "Yes" : "No");
			if(b.getRecord()!=null) {
				CheckOutEntry r = b.getRecord(); 
				br.setMemberId(r.getMember().getMemberId());
				br.setMemberName(r.getMember().getFirstName() + " " + r.getMember().getLastName());
				br.setDueBack(r.getDue());
				long days = LocalDateTime.from(LocalDateTime.now()).until(r.getDueData(), ChronoUnit.DAYS);
				if(days>0) 
					br.setDaysLeft(days + " days left");
				
				else 
					br.setDaysLeft("Overdue");
					
				
			}
			books.add(br);
		}
		
		ObservableList<BookRecord> lst=FXCollections.observableArrayList(books);
		tbcol1.setCellValueFactory(new PropertyValueFactory("bookISBN"));
		tbcol2.setCellValueFactory(new PropertyValueFactory("bookTitle"));
		tbcol3.setCellValueFactory(new PropertyValueFactory("copyNum"));
		tbcol4.setCellValueFactory(new PropertyValueFactory("isAvailable"));
		tbcol5.setCellValueFactory(new PropertyValueFactory("memberName"));
		tbcol6.setCellValueFactory(new PropertyValueFactory("memberId"));
		tbcol7.setCellValueFactory(new PropertyValueFactory("dueBack"));
		tbcol8.setCellValueFactory(new PropertyValueFactory("daysLeft"));
		
		tbcol8.setCellFactory(new Callback<TableColumn<BookRecord,String>, TableCell<BookRecord,String>>() {

			@Override
			public TableCell<BookRecord, String> call(TableColumn<BookRecord, String> arg0) {
				
				return new TableCell <BookRecord, String>(){
					  @Override
		                public void updateItem(String item, boolean empty) {
		                    super.updateItem(item, empty);
		                    if (!isEmpty()) {
		                        this.setTextFill(Color.BLACK);
		                        if(item.contains("Overdue")) 
		                            this.setTextFill(Color.RED);
		                        setText(item);
		                    }
		                }
				};
			}
		});
		
		tbView.setItems(lst);
		tbView.refresh();
		
	}

}
