package application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

import dataaccess.Auth;
import dataaccess.DataAccessFacade;
import dataaccess.User;

public class LoginController {
	
	@FXML
	private Button btn;
	@FXML
	private TextField txtLogin;
	@FXML
	private PasswordField txtPwd;
	@FXML
	private Label lblLogin;
	@FXML
	private Label lblPwd;
	
	public void initialize() 
	{
		lblLogin.setText(txtLogin.getText().isEmpty()?"*":"");
		txtLogin.textProperty().addListener((observable, oldValue, newValue) -> 
		{
			lblLogin.setText(txtLogin.getText().isEmpty()?"*":"");
		});
	
		lblPwd.setText(txtPwd.getText().isEmpty()?"*":"");
		txtPwd.textProperty().addListener((observable, oldValue, newValue) -> 
		{
			lblPwd.setText(txtPwd.getText().isEmpty()?"*":"");
		});
	}
		
	public void login(ActionEvent event) 
	{
		String name = txtLogin.getText();
		String pwd = txtPwd.getText();
	
		if(!(name.isEmpty()) && !(pwd.isEmpty()))
		{
			DataAccessFacade da=new DataAccessFacade();
			HashMap<String, User> hmap=da.readUserMap();
			if(hmap.containsKey(name))
			{
				User usr=hmap.get(name);
				if(usr.getPassword().equals(pwd))
				{
					Stage stage = (Stage) btn.getScene().getWindow();
				    stage.close();
				   	try 
					{
						FXMLLoader loader = new FXMLLoader(getClass().getResource("MainForm.fxml"));
						Parent root = (Parent)loader.load();
						MainController controller = (MainController)loader.getController();
						controller.setAuth(usr.getAuthorization());
						Scene scene = new Scene(root,800,600);
						scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
						Stage st=new Stage();
						st.setScene(scene);
						if(usr.getAuthorization()==Auth.ADMIN)
							st.setTitle("Admin user");
						else if(usr.getAuthorization()==Auth.LIBRARIAN)
							st.setTitle("Librarian user");
						else if(usr.getAuthorization()==Auth.BOTH)
								st.setTitle("Super user {Admin;Librarian}");
						st.getIcons().add(new Image("/data/library.png"));
						st.show();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
					return;
				}
			}
			
			MessageBox.Show("Login Failure","Invalid User Name or Password");
		}
		else 
		{
			MessageBox.Show("Login Failure","Field should not be empty");
		}
	}
	
}
