package application;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import business.LibraryMember;
import business.Book;
import business.BookCopy;
import business.CheckOutEntry;
import business.CheckoutRecord;
import dataaccess.Auth;
import dataaccess.DataAccessFacade;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainController {

	Auth userType;
	private DataAccessFacade da;

	@FXML
	private TabPane tabPane;
	
	@FXML
	private Tab tbBook;
	
	@FXML
	private Tab tbMember;
	
	@FXML
	private Tab tbCheckout;
	
	@FXML
	private Tab tbSearch;
	
	@FXML
	private Tab tbSearchBook;

	// --------------- Lib Member--------------------- 
	@FXML
	private TextField txtLibMemFilter;
	
	@FXML
	private TableView<LibraryMember> tbLibMemView;
	
	@FXML 
	private TableColumn<LibraryMember, String> tbLibMemcol1;
	
	@FXML 
	private TableColumn<LibraryMember, String> tbLibMemcol2;
	
	@FXML 
	private TableColumn<LibraryMember, String> tbLibMemcol3;
	
	@FXML 
	private TableColumn<LibraryMember, String> tbLibMemcol4;
	
	@FXML 
	private TableColumn<LibraryMember, String> tbLibMemcol5;
	
	@FXML
	private Button btnAddNewMember;
	@FXML
	private Button btnEditMember;
	@FXML
	private Button btnDeleteMember;
	
	private ObservableList<LibraryMember> lstLibMaster;
	
	// --------------- BOOK -------------------------
	
	@FXML
	private TextField txtBookFilter;
	
	@FXML
	private TableView<Book> tbBookView;
	
	@FXML 
	private TableColumn<Book, String> tbBookcol1;
	
	@FXML 
	private TableColumn<Book, String> tbBookcol2;
	
	@FXML 
	private TableColumn<Book, String> tbBookcol3;
	
	@FXML 
	private TableColumn<Book, String> tbBookcol4;
	
	@FXML 
	private TableColumn<Book, String> tbBookcol5;
	
	@FXML 
	private TableColumn<Book, String> tbBookcol6;
	
	@FXML
	private Button btnAddNewBook;
	@FXML
	private Button btnAddBookCopy;
	
	private ObservableList<Book> lstBookMaster;
	
	//checkout views
	@FXML
	private TextField memberID;
	@FXML
	private TextField bookNum;
	@FXML
	private Button checkBtn;
		
	//search by ID views
	@FXML
	private TextField searchId;
	@FXML
	private Button printBtn;
		
	//search book
	@FXML
	private TextField bookISBN;
	@FXML
	private Button searchBtn;
	
	public void initialize() 
	{
		da = new DataAccessFacade();
		libmember_initialize();
		book_initialize();
	}
	
	private void libmember_initialize()
	{
		tbLibMemcol1.setCellValueFactory(new PropertyValueFactory("memberId"));
		tbLibMemcol2.setCellValueFactory(new PropertyValueFactory("firstName"));
		tbLibMemcol3.setCellValueFactory(new PropertyValueFactory("lastName"));
		tbLibMemcol4.setCellValueFactory(new PropertyValueFactory("address"));
		tbLibMemcol5.setCellValueFactory(new PropertyValueFactory("telephone"));
		
		HashMap<String, LibraryMember> hmap=da.readMemberMap();
		if(hmap==null)
			return;
		this.lstLibMaster=FXCollections.observableArrayList(hmap.values());
		FilteredList<LibraryMember> filteredData = new FilteredList<>(this.lstLibMaster, p -> true);
		
		txtLibMemFilter.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(lib -> {
				// If filter text is empty, display all persons.
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				// Compare first name and last name of every person with filter text.
				String lowerCaseFilter = newValue.toLowerCase();
				
				if (lib.getFirstName().toLowerCase().contains(lowerCaseFilter)) 
				{
					return true; // Filter matches first name.
				} 
				else if (lib.getLastName().toLowerCase().contains(lowerCaseFilter)) 
				{
					return true; // Filter matches last name.
				}
				else if (lib.getTelephone().toLowerCase().contains(lowerCaseFilter)) 
				{
					return true; // Filter matches last name.
				}
				else if (lib.getAddress().toString().toLowerCase().contains(lowerCaseFilter)) 
				{
					return true; // Filter matches last name.
				}
				return false; // Does not match.
			});
		});

		SortedList<LibraryMember> sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(tbLibMemView.comparatorProperty());
		tbLibMemView.setItems(sortedData);
		tbLibMemView.refresh();
	}
	
	public void setAuth(Auth a)
	{
		this.userType=a;
		if(this.userType==Auth.LIBRARIAN)
		{
			tabPane.getTabs().remove(tbBook);
			tabPane.getTabs().remove(tbMember);
		}
		else if(this.userType==Auth.ADMIN)
		{
			tabPane.getTabs().remove(tbCheckout);
			tabPane.getTabs().remove(tbSearch);
			tabPane.getTabs().remove(tbSearchBook);
			this.libmember_initialize();
		}
	}
	
	@FXML
	public void AddNewMember(ActionEvent event) throws IOException 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AddMemberForm.fxml"));
		Parent root = (Parent)loader.load();
		AddMemberController controller = (AddMemberController)loader.getController();
		Scene scene = new Scene(root,600,600);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		Stage st=new Stage();
		st.setScene(scene);
		st.setTitle("Add new member");
		st.getIcons().add(new Image("/data/library.png"));
		st.initModality(Modality.APPLICATION_MODAL);
		st.showAndWait();
		if(controller.getResult())
			this.libmember_initialize();
	}
	
	@FXML
	public void EditMember(ActionEvent event) throws IOException 
	{
		if(tbLibMemView.getSelectionModel()!=null)
		{
			LibraryMember lib=tbLibMemView.getSelectionModel().getSelectedItem();
			if(lib!=null)
			{
				FXMLLoader loader = new FXMLLoader(getClass().getResource("AddMemberForm.fxml"));
				Parent root = (Parent)loader.load();
				AddMemberController controller = (AddMemberController)loader.getController();
				controller.EditMember(lib);
				Scene scene = new Scene(root,600,600);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				Stage st=new Stage();
				st.setScene(scene);
				st.setTitle("Edit member info");
				st.getIcons().add(new Image("/data/library.png"));
				st.initModality(Modality.APPLICATION_MODAL);
				st.showAndWait();
				if(controller.getResult())
					this.libmember_initialize();
				return;
			}
		}
		
		MessageBox.Show("Warning", "Member not selected");
	}
	
	@FXML
	public void DeleteMember(ActionEvent event) throws IOException 
	{
		if(tbLibMemView.getSelectionModel()!=null)
		{
			LibraryMember lib=tbLibMemView.getSelectionModel().getSelectedItem();
			if(lib!=null)
			{
				// Check here Member for checkout records
				List<CheckOutEntry> lstEntry = da.getCheckOutRecordById(lib.getMemberId()).getEntries();
				if(lstEntry.size()>0)
				{
					MessageBox.Show("Error", "Can't delete member from system. He/She needs to return books.");
					return;
				}
				
				if(da.removeLibraryMember(lib.getMemberId()))
					this.libmember_initialize();
				return;
			}
		}
		
		MessageBox.Show("Warning", "Member not selected");
	}

	
	private void book_initialize()
	{
		tbBookcol1.setCellValueFactory(new PropertyValueFactory("isbn"));
		tbBookcol2.setCellValueFactory(new PropertyValueFactory("Title"));
		tbBookcol3.setCellValueFactory(new PropertyValueFactory("maxCheckoutLength"));
		tbBookcol4.setCellValueFactory(new PropertyValueFactory("NumCopies"));
		tbBookcol5.setCellValueFactory(new PropertyValueFactory("AvailableCount"));
		tbBookcol6.setCellValueFactory(new PropertyValueFactory("AuthorsInfo"));
		
		HashMap<String, Book> map=da.readBooksMap();
		if(map==null)
			return;
			
		this.lstBookMaster=FXCollections.observableArrayList(map.values());
		FilteredList<Book> filteredData = new FilteredList<>(this.lstBookMaster, p -> true);
		
		txtBookFilter.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(book -> {
				// If filter text is empty, display all persons.
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				// Compare first name and last name of every person with filter text.
				String lowerCaseFilter = newValue.toLowerCase();
								
				if (book.getIsbn().toLowerCase().contains(lowerCaseFilter)) 
				{
					return true; // Filter matches first name.
				} 
				else if (book.getTitle().toLowerCase().contains(lowerCaseFilter)) 
				{
					return true; // Filter matches last name.
				}
				else if (book.getAuthors().toString().toLowerCase().contains(lowerCaseFilter)) 
				{
					return true; // Filter matches last name.
				}
				
				return false; // Does not match.
			});
		});

		SortedList<Book> sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(tbBookView.comparatorProperty());
		tbBookView.setItems(sortedData);
		tbBookView.refresh();
	} 
	
	
	@FXML
	public void AddNewBook(ActionEvent event) throws IOException 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("NewBook.fxml"));
		Parent root = (Parent)loader.load();
		Scene scene = new Scene(root,600,500);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		Stage st=new Stage();
		st.setScene(scene);
		st.setTitle("Add new book");
		st.getIcons().add(new Image("/data/library.png"));
		st.initModality(Modality.APPLICATION_MODAL);
		st.showAndWait();
		this.book_initialize();
	}
	
	
	@FXML
	public void AddBookCopy(ActionEvent event) throws IOException 
	{
		if(tbBookView.getSelectionModel()!=null)
		{
			Book book=tbBookView.getSelectionModel().getSelectedItem();
			if(book!=null)
			{
				FXMLLoader loader = new FXMLLoader(getClass().getResource("NewBook.fxml"));
				Parent root = (Parent)loader.load();
				BookController controller = (BookController)loader.getController();
				controller.EditCopy(book);
				Scene scene = new Scene(root,600,500);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				Stage st=new Stage();
				st.setScene(scene);
				st.setTitle("Add book copy");
				st.getIcons().add(new Image("/data/library.png"));
				st.initModality(Modality.APPLICATION_MODAL);
				st.showAndWait();
				this.book_initialize();
				return;
			}
		}
		
		MessageBox.Show("Warning", "Book not selected");
	}
	
	@FXML
	private void checkOutBook(ActionEvent event) {
		String memberIDVal = memberID.getText();
		String bookNumVal = bookNum.getText();
		if(!memberIDVal.isEmpty() && !bookNumVal.isEmpty()) {
			HashMap<String, LibraryMember> membersMap=da.readMemberMap();
			HashMap<String, Book> booksMap=da.readBooksMap();
			if(membersMap.containsKey(memberIDVal)){
				if(booksMap.containsKey(bookNumVal)) {
					Book book = booksMap.get(bookNumVal);
					if(book.isAvailable()) {
						LibraryMember member = membersMap.get(memberIDVal);
						for(BookCopy b: book.getCopies()) {
							if(b.isAvailable()) {
								b.changeAvailability();
								CheckOutEntry entry = 
										new CheckOutEntry(member, book, LocalDateTime.now(), LocalDateTime.now().plusDays(book.getMaxCheckoutLength()), String.valueOf(b.getCopyNum()));
								b.setRecord(entry);
								book.updateCopies(b);
								entry.setBook(book);
//								da.saveBookCopies(book);
								booksMap.put(bookNumVal, book);
								DataAccessFacade.updateBooks(booksMap);
								
								CheckoutRecord record = da.getCheckOutRecordById(member.getMemberId());
								record.addEntry(entry);
								da.updateCheckoutRecord(record);
								showRecordInfo(memberIDVal);
								
								this.book_initialize();
								return;
							}
						}
						
					}else {
						MessageBox.Show("Error","Requested book is not available");
					}
				}else {
					MessageBox.Show("Error","Requested book is not found");
					}
			}else {
				MessageBox.Show("Error","Requested member is not found");
			}
		}else {
			MessageBox.Show("Error","Fields should not be empty");
		}
	}
	
	private void showRecordInfo(String memberId) {
		memberID.setText("");
		bookNum.setText("");
		searchId.setText("");
		try 
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("CheckoutRecordForm.fxml"));
			Parent root = (Parent)loader.load();
			CheckoutRecordController controller = (CheckoutRecordController)loader.getController();
			Scene scene = new Scene(root,600,500);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			Stage st=new Stage();
			st.setScene(scene);
			st.setTitle("Library Member#"+memberId);
			st.getIcons().add(new Image("/data/library.png"));
			st.initModality(Modality.APPLICATION_MODAL);
			controller.setMemberId(memberId);
			controller.setTableViewValues();
			st.showAndWait();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void printRecords() {
		String memberIDVal = searchId.getText();
		if(!memberIDVal.isEmpty()) {
			HashMap<String, LibraryMember> membersMap=da.readMemberMap();
			if(membersMap.containsKey(memberIDVal)){
				
				List<CheckOutEntry> entries = da.getCheckOutRecordById(memberIDVal).getEntries();
				for(CheckOutEntry entry: entries) 
					System.out.println(entry);
					
				showRecordInfo(memberIDVal);
				
			}
			else {
				MessageBox.Show("Error","Requested member is not found");
			}
			}
		else {
			MessageBox.Show("Error","Fields should not be empty");
		}
	}
		
	@FXML
	private void searchBook() 
	{
		String bookId = bookISBN.getText();
		if(!bookId.isEmpty()) 
		{
			HashMap<String, Book> booksMap=da.readBooksMap();
			if(booksMap.containsKey(bookId))
			{
				Book b = booksMap.get(bookId);
				showBookInfo(b);
			}
			else 
			{
				MessageBox.Show("Error","Requested book is not found");
			}
		}
		else 
		{
			MessageBox.Show("Error","Fields should not be empty");
		}
	}
	
	private void showBookInfo(Book book)
	{
		bookISBN.setText("");
		try 
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("BookForm.fxml"));
		    Parent root = (Parent)loader.load();
			BookSearchController controller = (BookSearchController)loader.getController();
			Scene scene = new Scene(root,600,500);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			Stage st=new Stage();
			st.setScene(scene);
			st.setTitle(book.getTitle());
			st.getIcons().add(new Image("/data/library.png"));
			st.initModality(Modality.APPLICATION_MODAL);
			controller.setTableViewValues(book);
			st.showAndWait();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
