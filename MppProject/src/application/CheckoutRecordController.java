package application;

import java.util.List;
import business.CheckOutEntry;
import dataaccess.DataAccessFacade;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class CheckoutRecordController {
	private String memberId;
	
	@FXML
	private TableView<CheckOutEntry> tbView1;
	
	@FXML 
	private TableColumn<CheckOutEntry, String> tbcol01;
	
	@FXML 
	private TableColumn<CheckOutEntry, String> tbcol02;
	
	@FXML 
	private TableColumn<CheckOutEntry, String> tbcol03;
	
	@FXML 
	private TableColumn<CheckOutEntry, String> tbcol04;
	
	public void initialize() 
	{
		
	}
	
	public void setMemberId(String memberId) 
	{
		this.memberId = memberId;
	}
	
	public void setTableViewValues() 
	{
		DataAccessFacade da = new DataAccessFacade();
		List<CheckOutEntry> entries = da.getCheckOutRecordById(memberId).getEntries();
		
		ObservableList<CheckOutEntry> lst=FXCollections.observableArrayList(entries);
		tbcol01.setCellValueFactory(new PropertyValueFactory("bookTitle"));
		tbcol02.setCellValueFactory(new PropertyValueFactory("checkOut"));
		tbcol03.setCellValueFactory(new PropertyValueFactory("due"));
		tbcol04.setCellValueFactory(new PropertyValueFactory("copyNum"));
		tbView1.setItems(lst);
		tbView1.refresh();

	}

}
