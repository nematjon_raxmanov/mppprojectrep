package application;

import business.Address;
import business.Author;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AuthorController {
	
	@FXML
	private Button btnAddAuthor;
		@FXML
	private TextField txtFirstName;
	@FXML
	private TextField txtLastName;
	@FXML
	private TextField txtPhone;
	@FXML
	private TextField txtBio;
	@FXML
	private TextField txtStreet;
	@FXML
	private TextField txtCity;
	@FXML
	private TextField txtState;
	@FXML
	private TextField txtZipCode;
	@FXML
	private Label lblError;

	Author auth;
	
	public void initialize() 
	{
		this.lblError.setText("");
	}
	
	public void AddAuthor(ActionEvent event) 
	{
		String fname=txtFirstName.getText();
		if(fname.isEmpty() || fname.isBlank())
		{
			lblError.setText("Please enter correct first name!");
			return;
		}
		
		String lname=txtLastName.getText();
		if(lname.isEmpty() || lname.isBlank())
		{
			lblError.setText("Please enter correct last name!");
			return;
		}
		
		String phone=txtPhone.getText();
		if(phone.isEmpty() || phone.isBlank() || phone.length()!=12)
		{
			lblError.setText("Please enter correct phone number! Format=XXX-XXX-XXXX");
			return;
		}
		
		String street=txtStreet.getText();
		if(street.isEmpty() || street.isBlank())
		{
			lblError.setText("Please enter correct street!");
			return;
		}
		
		String city=txtCity.getText();
		if(city.isEmpty() || city.isBlank())
		{
			lblError.setText("Please enter correct city!");
			return;
		}
		
		String state=txtState.getText();
		if(state.isEmpty() || state.isBlank())
		{
			lblError.setText("Please enter correct state!");
			return;
		}
		
		String zip=txtZipCode.getText();
		if(zip.isEmpty() || zip.isBlank() || zip.matches("\\d{10}|\\d{11}"))
		{
			lblError.setText("Please enter correct zip number!");
			return;
		}
		
		auth=new Author(fname, lname, phone, new Address(street, city, state, zip),txtBio.getText());
		Stage stage = (Stage) btnAddAuthor.getScene().getWindow();
	    stage.close();
	}
	
	public Author getAuthor()
	{
		return this.auth;
	}
}
