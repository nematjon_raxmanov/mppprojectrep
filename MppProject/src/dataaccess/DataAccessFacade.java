package dataaccess;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;

import business.Book;
import business.CheckoutRecord;
import business.LibraryMember;


public class DataAccessFacade implements DataAccess {
	
	enum StorageType 
	{
		BOOKS, MEMBERS, USERS, CHECKOUT_RECORDS;
	}
	
	public static final String OUTPUT_DIR = System.getProperty("user.dir") + "/src/dataaccess/storage";
	public static final String DATE_PATTERN = "MM/dd/yyyy";
	
	public String generateMemberId()
	{
		HashMap<String, LibraryMember> map=readMemberMap();
		int max_id=0;
		for(LibraryMember lib:map.values())
		{
			String id=lib.getMemberId();
			int x= Integer.valueOf(id).intValue();
			if(x>max_id)
				max_id=x;
		}
		
		return String.valueOf(++max_id);
	}
	
	//implement: other save operations
	public void saveNewMember(LibraryMember member) 
	{
		HashMap<String, LibraryMember> mems = readMemberMap();
		String memberId = member.getMemberId();
		mems.put(memberId, member);
		saveToStorage(StorageType.MEMBERS, mems);	
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, LibraryMember> readMemberMap() 
	{
		//Returns a Map with name/value pairs being
		//   memberId -> LibraryMember
		return (HashMap<String, LibraryMember>) readFromStorage(StorageType.MEMBERS);
	}
	
	public boolean removeLibraryMember(String memberId)
	{
		HashMap<String, LibraryMember> mems = readMemberMap();
		if(mems.containsKey(memberId))
		{
			mems.remove(memberId);
			saveToStorage(StorageType.MEMBERS, mems);	
			return true;
		}
		
		return false;
	}
	
	public boolean editLibraryMember(LibraryMember member)
	{
		HashMap<String, LibraryMember> mems = readMemberMap();
		String memberId=member.getMemberId();
		if(mems.containsKey(memberId))
		{
			mems.remove(memberId);
			mems.put(memberId, member);
			saveToStorage(StorageType.MEMBERS, mems);	
			return true;
		}
		
		return false;
	}
		
	@SuppressWarnings("unchecked")
	public HashMap<String,Book> readBooksMap() 
	{
		//Returns a Map with name/value pairs being
		//   isbn -> Book
		return (HashMap<String,Book>) readFromStorage(StorageType.BOOKS);
	}
	
	public boolean saveNewBook(Book book)
	{
		HashMap<String,Book> map=readBooksMap(); 
		if(!map.containsKey(book.getIsbn()))
		{
			map.put(book.getIsbn(), book);
			saveToStorage(StorageType.BOOKS, map);	
			return true;
		}
		
		return false;
	}
	
	public boolean saveBookCopies(Book book)
	{
		HashMap<String,Book> map=readBooksMap(); 
		if(map.containsKey(book.getIsbn()))
		{
			map.remove(book.getIsbn());
			
			map.put(book.getIsbn(), book);
			saveToStorage(StorageType.BOOKS, map);	
			return true;
		}
		
		return false;
	}
	
	public boolean isISBNExist(String str)
	{
		HashMap<String,Book> map=readBooksMap(); 
		return map.containsKey(str);
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, User> readUserMap() {
		//Returns a Map with name/value pairs being
		//   userId -> User
		return (HashMap<String, User>)readFromStorage(StorageType.USERS);
	}
	
	
	/////load methods - these place test data into the storage area
	///// - used just once at startup  
	
		
	static void loadBookMap(List<Book> bookList) {
		HashMap<String, Book> books = new HashMap<String, Book>();
		bookList.forEach(book -> books.put(book.getIsbn(), book));
		saveToStorage(StorageType.BOOKS, books);
	}
	
	static void loadUserMap(List<User> userList) {
		HashMap<String, User> users = new HashMap<String, User>();
		userList.forEach(user -> users.put(user.getId(), user));
		saveToStorage(StorageType.USERS, users);
	}
 
	static void loadMemberMap(List<LibraryMember> memberList) {
		HashMap<String, LibraryMember> members = new HashMap<String, LibraryMember>();
		memberList.forEach(member -> members.put(member.getMemberId(), member));
		saveToStorage(StorageType.MEMBERS, members);
	}
	
	static void saveToStorage(StorageType type, Object ob) 
	{
		ObjectOutputStream out = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, type.toString());
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(ob);
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
	}
	
	static Object readFromStorage(StorageType type) 
	{
		ObjectInputStream in = null;
		Object retVal = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, type.toString());
			in = new ObjectInputStream(Files.newInputStream(path));
			retVal = in.readObject();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return retVal;
	}
			
	final static class Pair<S,T> implements Serializable{
		
		S first;
		T second;
		Pair(S s, T t) {
			first = s;
			second = t;
		}
		@Override 
		public boolean equals(Object ob) {
			if(ob == null) return false;
			if(this == ob) return true;
			if(ob.getClass() != getClass()) return false;
			@SuppressWarnings("unchecked")
			Pair<S,T> p = (Pair<S,T>)ob;
			return p.first.equals(first) && p.second.equals(second);
		}
		
		@Override 
		public int hashCode() {
			return first.hashCode() + 5 * second.hashCode();
		}
		@Override
		public String toString() {
			return "(" + first.toString() + ", " + second.toString() + ")";
		}
		private static final long serialVersionUID = 5399827794066637059L;
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String,CheckoutRecord> readRecordsMap() {
		//Returns a Map with name/value pairs being
		//   memberId -> CheckoutRecord
		StorageType type = StorageType.CHECKOUT_RECORDS;
		HashMap<String, CheckoutRecord> records;
		if(isFileExist(type)) {
			File f = new File(OUTPUT_DIR, type.toString());
			if(f.length()==0) {
				records = new HashMap<String, CheckoutRecord>();
			}
			else {
				records = (HashMap<String, CheckoutRecord>)readFromStorage(type);
				if(records==null) {
					records = new HashMap<String, CheckoutRecord>();
				}
			}
			
		}else {
			records = new HashMap<String, CheckoutRecord>();
		}
		return records;
	}
	
	public CheckoutRecord getCheckOutRecordById(String id) {
		CheckoutRecord record;
		HashMap<String, CheckoutRecord> records = readRecordsMap();
		if(records.containsKey(id))
			record = records.get(id);
		else
			record = new CheckoutRecord(id);
		return record;
	}
	
	public void updateCheckoutRecord(CheckoutRecord record) {
		HashMap<String, CheckoutRecord> records = readRecordsMap();
		records.put(record.getMemberId(), record);
		saveToStorage(StorageType.CHECKOUT_RECORDS, records);
	}
	
	static boolean isFileExist(StorageType type) {
		File f = new File(OUTPUT_DIR, type.toString());
		if(f.exists() && !f.isDirectory())
			return true;
		else
			try {
				f.createNewFile();
	        }
	        catch (Exception e) {
	            System.err.println(e);
	        }
		return false;
	}
	
public static void updateBooks(HashMap<String, Book> books) {
		
		saveToStorage(StorageType.BOOKS, books);
	}
	
}
