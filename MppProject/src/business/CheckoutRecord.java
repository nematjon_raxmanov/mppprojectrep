package business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

final public class CheckoutRecord implements Serializable {

	private static final long serialVersionUID = -1984035285805331755L;
	private String memberId;
	private List<CheckOutEntry>entries = new ArrayList<>();
	
	public CheckoutRecord(String memberId) {
		this.memberId = memberId;
	}
	
	public String getMemberId() {
		return memberId;
	}

	public List<CheckOutEntry> getEntries() {
		return entries;
	}
	
	public void addEntry(CheckOutEntry record) {
		entries.add(record);
	}
	
	
	
}
