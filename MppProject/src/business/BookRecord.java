package business;

import java.io.Serializable;

public class BookRecord implements Serializable {

	private static final long serialVersionUID = 2209687246484619591L;
	
	private String bookISBN = "";
	private String bookTitle = "";
	private String copyNum = "";
	private String isAvailable = "";
	private String memberName = "";
	private String memberId = "";
	private String dueBack = "";
	private String daysLeft = "";
	public BookRecord(String bookISBN, String bookTitle, String copyNum, String isAvailable) {
		super();
		this.bookISBN = bookISBN;
		this.bookTitle = bookTitle;
		this.copyNum = copyNum;
		this.isAvailable = isAvailable;
	}
	
	public String getBookISBN() {
		return bookISBN;
	}
	
	public String getBookTitle() {
		return bookTitle;
	}
	
	public String getCopyNum() {
		return copyNum;
	}
	
	public String getIsAvailable() {
		return isAvailable;
	}
	
	public String getMemberName() {
		return memberName;
	}
	
	public String getMemberId() {
		return memberId;
	}

	public String getDueBack() {
		return dueBack;
	}

	public String getDaysLeft() {
		return daysLeft;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public void setDueBack(String dueBack) {
		this.dueBack = dueBack;
	}
	public void setDaysLeft(String daysLeft) {
		this.daysLeft = daysLeft;
	}
	
	

}
