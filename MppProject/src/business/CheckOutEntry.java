package business;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CheckOutEntry implements Serializable {

	private static final long serialVersionUID = -1357394649317245169L;
	private String bookTitle;
	private String checkOut;
	private String due;
	private String copyNum;
	
	private LibraryMember member;
	private Book book;
	private LocalDateTime checkOutDate;
	private LocalDateTime dueData;
	private double fine = 0d;
	private LocalDateTime finePaymentDate;
	
	
	public CheckOutEntry(LibraryMember member, Book book, LocalDateTime dateOfCheckout, LocalDateTime dueData, String copyNum) {
		this.member = member;
		this.book = book;
		this.checkOutDate = dateOfCheckout;
		this.dueData = dueData;
		this.copyNum = copyNum;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");

		bookTitle = book.getTitle();
		checkOut = dateOfCheckout.format(formatter);
		due = dueData.format(formatter);
	}
	
	public LibraryMember getMember() {
		return member;
	}
	
	public Book getBook() {
		return book;
	}
	
	public LocalDateTime getDateOfCheckout() {
		return checkOutDate;
	}
	
	public LocalDateTime getDueData() {
		return dueData;
	}
	
	public double getFine() {
		return fine;
	}
	
	public void setFine(double fine) {
		this.fine = fine;
	}
	
	public LocalDateTime getFinePaymentDate() {
		return finePaymentDate;
	}
	
	public void setFinePaymentDate(LocalDateTime finePaymentDate) {
		this.finePaymentDate = finePaymentDate;
	}

	public String getBookTitle() {
		return bookTitle;
	}
	
	public String getCheckOut() {
		return checkOut;
	}


	public String getDue() {
		return due;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	public String getCopyNum() {
		return copyNum;
	}

	@Override
	public String toString() {
		return "Book: " + book.getTitle() + " | " + "Checkout Date: " + 
				getCheckOut() + " | " + "Due Date: " + getDue() + " | " + "Copy Number: " + getCopyNum();
	}

	

}
